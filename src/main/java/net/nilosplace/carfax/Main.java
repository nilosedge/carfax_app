package net.nilosplace.carfax;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;

import lombok.extern.log4j.Log4j2;
import net.nilosplace.lib_java_carfax.CarfaxAPI;
import net.nilosplace.lib_java_carfax.CarfaxListing;

@Log4j2
public class Main {

	public static void main(String[] args) throws Exception {
		
		CarfaxAPI api = new CarfaxAPI();
		String make = "Toyota";
		String model = "4Runner";
		api.setZip("52501");
		api.setRadius(500);
		api.setMake(make);
		api.setModel(model);
		api.setNoAccidents(true);
		api.setOneOwner(true);
		
		List<CarfaxListing> results = api.searchVehicles();
		
		HashSet<String> set = new HashSet<String>();
		
		HashSet<Integer> miles = new HashSet<Integer>();
		
		File outfile = new File(make + "-" + model + ".csv");
		PrintWriter writer = new PrintWriter(outfile);
		writer.println("Vin,Millage,Price,Year");
		
		for(CarfaxListing l: results) {
			if(l.getCurrentPrice() > 0) {
				
				while(miles.contains(l.getMileage())) {
					l.setMileage(l.getMileage() + 1);
				}
				
				miles.add(l.getMileage());
				
				if(!set.contains(l.getVin())) {
					System.out.println(l.getVin() + "," + l.getMileage() + "," + l.getCurrentPrice() + "," + l.getYear());
					writer.println(l.getVin() + "," + l.getMileage() + "," + l.getCurrentPrice() + "," + l.getYear());
					set.add(l.getVin());
				}
				//log.debug("Link: " + l.getLink() + " " + l);
				
			}
		}
		writer.close();

	}

}
